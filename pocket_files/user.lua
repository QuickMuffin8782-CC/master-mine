-- CONTINUOUSLY AWAIT USER INPUT AND PLACE IN TABLE
print("Mastermine remote user control")
while true do
    term.setTextColor(colors.yellow)
    write("usr:hub> ")
    term.setTextColor(colors.white)
    rednet.broadcast(read(), 'user_input')
    print("Sent command to hub successfully")
end
